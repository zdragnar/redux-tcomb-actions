import t from 'tcomb';

export default t.irreducible(
    'ReduxAction',
    a => a instanceof Object && t.String.is(a.type)
);
