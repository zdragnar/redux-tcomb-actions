import t from 'tcomb';
import ReduxAction from './ActionIrreducible';

export default function generateActionCreator(PayloadType, actionConstant, actionCreatorFunction) {
    let actionCreator = actionCreatorFunction;

    if (t.Nil.is(actionCreator)) {
        actionCreator = function actionCreatorFn(...args) {
            if (process.env.NODE_ENV !== 'production') {
                t.assert(args.length < 2, `The action creator ${actionConstant} is using the default implementation but you are passing in ${args.length} arguments (expected only the payload argument)`);
            }

            return {
                payload: PayloadType(args[0]),
            };
        };
    }

    // arguments type-checking
    if (process.env.NODE_ENV !== 'production') {
        t.assert(t.isType(PayloadType), 'Invalid argument PayloadType supplied to generateActionCreator(PayloadType, actionConstant, actionCreator) (expected a tcomb type)');
        t.assert(t.String.is(actionConstant), 'Invalid argument actionConstant supplied to generateActionCreator(PayloadType, actionConstant, actionCreator) (expected a string)');
        t.assert(t.Function.is(actionCreator), 'Invalid argument actionCreator supplied to generateActionCreator(PayloadType, actionConstant, actionCreator) (expected a function)');
    }

    const Action = t.refinement(ReduxAction,
        a => a.type === actionConstant && PayloadType.is(a.payload),
        actionConstant
    );

    function newActionCreator(...args) {
        const result = actionCreator(...args);
        result.type = actionConstant;

        return Action(result);
    }

    newActionCreator.displayName = `ActionCreator(${actionConstant})`; // provides nice messages in the Chrome's Call Stack Panel if something goes wrong
    newActionCreator.actionName = actionConstant;
    newActionCreator.type = Action;
    newActionCreator.payloadType = PayloadType;

    return newActionCreator;
}

generateActionCreator.ReduxActionIrreducible = ReduxAction;
