import ava from 'ava';
import t from 'tcomb';

import generateActionCreator from '../source/generateActionCreator';

const UserCredentials = t.struct({
    username: t.String,
    password: t.String,
    remeberMe: t.maybe(t.Boolean),
});

const LOGIN_START_ACTION = 'login-start-action';

ava('generateActionCreator makes an action creator, and exposes the action constant and payload type', test => {
    const loginStartActionCreator = generateActionCreator(
        UserCredentials,
        LOGIN_START_ACTION,
        credentials => ({ payload: new UserCredentials(credentials) })
    );

    test.true(loginStartActionCreator.actionName === LOGIN_START_ACTION);
    test.true(loginStartActionCreator.payloadType === UserCredentials);

    const loginStartAction = loginStartActionCreator({
        username: 'dan',
        password: 'test',
    });

    test.true(loginStartActionCreator.type.is(loginStartAction));
});
