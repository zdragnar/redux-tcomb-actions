import test from 'ava';

import ReduxAction from '../source/ActionIrreducible';

test('ActionIrreducible', t => {
    t.doesNotThrow(() => {
        ReduxAction({ type: 'test-action-type' });
    });

    t.throws(() => {
        ReduxAction({ payload: 1 });
    }, t.Error);
});
