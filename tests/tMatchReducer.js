import ava from 'ava';
import t from 'tcomb';

import {
    loginStart,
    loginSuccess,
    loginFail,
    logout,
    sessionTimeout
} from '../examples/Actions';

import {
    UserCredentials,
    User,
} from '../examples/types';

import reducer from '../examples/tMatchReducer';

ava('Actions can be used in a t.match for reducers with simple logic', test => {
    const loadingAction = loginStart(new UserCredentials({ username: 'dan', password: 'test' }));
    const loadingState = reducer(undefined, loadingAction);

    test.true(loadingState.isLoading);
    test.is(loadingState.user, null);
    test.is(loadingState.error, null);

    const successAction = loginSuccess({ username: 'dan', created: new Date() });
    const successState = reducer(loadingState, successAction);

    test.false(successState.isLoading);
    test.is(successState.error, null);
    test.true(User.is(successState.user));

    const failAction = loginFail(new Error('some network error'));
    const failState = reducer(loadingState, failAction);

    test.false(failState.isLoading);
    test.true(t.Error.is(failState.error));
    test.is(failState.user, null);

    const logoutAction = logout();
    const logoutState = reducer(successState, logoutAction);
    test.false(logoutState.isLoading);
    test.is(logoutState.error, null);
    test.is(logoutState.user, null);

    const sessionTimeoutAction = sessionTimeout();
    const sessionTimeoutState = reducer(successState, sessionTimeoutAction);
    test.false(sessionTimeoutState.isLoading);
    test.is(sessionTimeoutState.error, null);
    test.is(sessionTimeoutState.user, null);
});
