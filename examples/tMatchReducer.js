import t from 'tcomb';

import {
    loginStart,
    loginSuccess,
    loginFail,
    logout,
    sessionTimeout,
} from './Actions';

// generated action.type properties are refinements of ActionIrreducible
// so they're safe to create union combinator types to be used in t.match
const endUserSession = t.union([logout.type, sessionTimeout.type], 'endUserSession');

import { User } from './types';

const StateStruct = t.struct({
    isLoading: t.Boolean,
    error: t.maybe(t.Error),
    user: t.maybe(User),
});

const initialState = new StateStruct({
    isLoading: false,
});

const mergeState = (state, updates) => StateStruct.update(state, { $merge: updates });

const reducer = (state = initialState, action) =>  t.match(action,
    // union type of logout and sessionTimeout
    endUserSession, () => initialState,

    loginStart.type, () => mergeState(state, { isLoading: true, error: null }),

    loginFail.type, () => mergeState(
        state,
        { isLoading: false, error: action.payload, user: null }
    ),

    loginSuccess.type, () => mergeState(
        state,
        { isLoading: false, user: action.payload, error: null }
    ),

    // don't forget to handle other actions!
    t.Any, () => state
);

export default reducer;
