import t from 'tcomb';

export const UserCredentials = t.struct({
    username: t.String,
    password: t.String,
});

export const User = t.struct({
    username: t.String,
    created: t.Date,
});
