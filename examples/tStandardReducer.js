import t from 'tcomb';

import {
    loginStart,
    loginSuccess,
    loginFail,
    logout,
} from './Actions';

import { User } from './types';

const StateStruct = t.struct({
    isLoading: t.Boolean,
    error: t.maybe(t.Error),
    user: t.maybe(User),
});

const initialState = new StateStruct({
    isLoading: false,
});

const mergeState = (state, updates) => StateStruct.update(state, { $merge: updates });

export default function reducer(state = initialState, action) {
    let newState;

    switch (action.type) {
        case loginStart.actionName:
            newState = mergeState(state, { isLoading: true, error: null });
            break;
        case loginSuccess.actionName:
            newState = mergeState(state, { isLoading: false, user: action.payload, error: null });
            break;
        case loginFail.actionName:
            newState = mergeState(state, { isLoading: false, error: action.payload, user: null });
            break;

        // note that sharing behavior between actions can be brittle
        // compared to the tMatchReducer example
        case logout.actionName:
        case sessionTimeout.actionName:
            newState = initialState;
            break;
        default:
            // don't forget to handle other actions!
            newState = state;
    }

    return newState;
}
