import t from 'tcomb';

import generateActionCreator from '../source/generateActionCreator';

import {
    UserCredentials,
    User,
} from './types';

const LOGIN_START = 'login-start';

export const loginStart = generateActionCreator(
    UserCredentials,
    LOGIN_START,
    // note: the payload property must be of the UserCredentials type, since
    // in this example, the action creator isn't validating on its own- see in the
    // unit tests for either reducer
    credentials => ({ payload: credentials })
);

const LOGIN_SUCCESS = 'login-success';

export const loginSuccess = generateActionCreator(
    User,
    LOGIN_SUCCESS,
    serverResponse => ({ payload: new User(serverResponse) })
);

const LOGIN_FAIL = 'login-fail';

export const loginFail = generateActionCreator(
    t.Error,
    LOGIN_FAIL,
    serverResponse => ({ payload: serverResponse })
);

const LOGOUT = 'logout';

export const logout = generateActionCreator(
    t.Nil,
    LOGOUT
);


const SESSION_TIMEOUT = 'session-timeout';
export const sessionTimeout = generateActionCreator(
    t.Nil,
    SESSION_TIMEOUT
);
