# redux-tcomb-actions


This module presents a simple way to generate action creators that provide a bit of type safety via tcomb.

The default export:
```javascript
function generateActionCreator(PayloadType : [tcomb type], actionConstant : String, ?actionCreator : Function)
```

also has the following property:

```javascript
generateActionCreator.ReduxActionIrreducible =  t.irreducible(
    'ReduxAction',
    a => a instanceof Object && t.String.is(a.type)
);
```

Each action creator generated will have a `type` refinement of this irreducible:

```javascript
t.refinement(ReduxActionIrreducible,
    a => a.type === actionConstant && PayloadType.is(a.payload),
    actionConstant
);
```

## Why use this module?
Reducer logic that depends on matching against strings is inevitably fragile, and the [tcomb](https://github.com/gcanti/tcomb) library provides some nice mechanisms for making actions and reducers safer, as well as simplifying reducer logic.

By adding tcomb metadata to action creators, reducers can leverate pattern matching, unions and refinements of actions to remove branching logic from reducer action handlers.  See the examples/tMatchReducer.js file for an example.


## Usage

Take the following example:

```javascript
const UserCredentials = t.struct({
    username: t.String,
    password: Password
});

const loginStart = generateActionCreator(
    UserCredentials, // the tcomb type to validate the payload
    'login-started', // the action "type" property
    uc => ({ payload: uc }) // what you would normally use for an action creator, except you won't be specifying the "type" property
);

loginStart.actionName  // 'login-started'
loginStart.payloadType // UserCredentials
loginStart.type        // A refinement of ActionIrreducible that has a guard function validating the action's payload property conforms to the PayloadType

const loginStartAction = loginStart({ username: 'test', password: 'password' });

loginStartAction // { type: 'login-start', payload: {username: 'test', password: 'password'}}
```

The result of calling `generateActionCreator` is an action creator function, with three properties that provide some introspection capability; the most useful of which is likely the action creator's `type` property: a special refinement of `ActionIrreducible`, a custom tcomb type that validates an action's type and payload properties.

To see how these are used, take a look at the `examples` folder and the `tests` folder.  There are two examples of reducers: one using tcomb pattern matching on the action creator tcomb type, and another using standard switching on the action type string.
